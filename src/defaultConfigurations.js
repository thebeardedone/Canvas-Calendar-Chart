/**
 * Created by Ivo Zeba on 5/14/17.
 *
 * Default configuration object
 */

export const defaultConfig = {
    color: {
        missingValue: "#000000",
        missingDate: "#FFFFFF",
        dateOutline: "#f1f1f1",
        monthOutline: "#000000",
        range: ["#D7191C", "#FDAE61", "#FFFFBF", "#A6D96A", "#1A9641"],
    },
    constants: {
        dayInMs: 86400000
    },
    chart: {
        width: 1500,
        height: 140,
        skipWeekend: false
    },
    data: [],
    events: {
        onClick: null,
        onHover: {
            date: null,
            value: null
        }
    },
    style: {
        squareSideLength: 17,
        xMargin: 30,
        yMargin: 17
    },
    text: {
        font: "Sans-serif",
        fontSize: 10,
        months: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        weekDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    }
};