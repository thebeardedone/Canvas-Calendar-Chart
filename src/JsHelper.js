/**
 * Created by Ivo Zeba on 5/14/17.
 *
 * Common javascript helper functions
 */

export class JsHelper {
    /**
     * Returns the number of days within a month
     *
     * @param month
     * @param year
     * @return {number}
     */
    static getDaysInMonth(month, year) {
        return new Date(year, month + 1, 0).getDate();
    };

    /**
     * Returns the minimum value of a specified attribute in an object array
     *
     * @param data Object array
     * @param attribute Attribute of the object for which the minimum will be calculated for
     * @return {*}
     */
    static minObjectValue(data, attribute) {
        return Math.min.apply(Math, data.map(function (object) {
            return object[attribute];
        }));
    }

    /**
     * Returns the maximum value of a specified attribute in an object array
     *
     * @param data Object array
     * @param attribute Attribute of the object for which the maximum will be calculated for
     * @return {*}
     */
    static maxObjectValue(data, attribute) {
        return Math.max.apply(Math, data.map(function (object) {
            return object[attribute];
        }));
    }

    /**
     * In place sort of an object array by date
     *
     * @param data Object array with an object that has the attribute date defined
     */
    static sortObjectArrayByDate(data) {
        data.sort(function (a, b) {
            let dateA = new Date(a.date);
            let dateB = new Date(b.date);

            if (dateA.getTime() < dateB.getTime())
                return -1;
            if (dateA.getTime() > dateB.getTime())
                return 1;
            return 0;
        });
    }

    /**
     * Returns the number of days between two dates
     *
     * @param date1
     * @param date2
     * @return {number}
     */
    static getNumberOfDaysBetweenDates(date1, date2) {
        let endDate = new Date(date2);
        let startDate = new Date(date1);
        return Math.abs(endDate.getTime() - startDate.getTime())/86400000;
    }

    /**
     * Returns the number of months between two dates
     *
     * @param date1
     * @param date2
     * @return {number}
     */
    static getNumberOfMonthsBetweenDates(date1, date2) {
        let endDate = new Date(date2);
        let startDate = new Date(date1);
        return endDate.getMonth() - startDate.getMonth() + 12 * (endDate.getFullYear() - startDate.getFullYear());
    }

    /**
     * Returns a string formatted date
     *
     * @param date
     * @param months
     * @return {string}
     */
    static getDateString(date, months) {
        let jsDate = new Date(date);
        return months[jsDate.getMonth()] + ". " + jsDate.getDate() + " " + jsDate.getFullYear();
    }
}
