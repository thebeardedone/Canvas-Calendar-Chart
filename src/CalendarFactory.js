/**
 * Created by Ivo Zeba on 5/14/17.
 *
 * Factory used to create calendar instances
 */

import { Calendar } from "./Calendar";

export class CalendarFactory {
    static createChart(config) {
        return new Calendar(config);
    }
}

// Make CalendarFactory available in the global scope
window.CalendarFactory = CalendarFactory;