/**
 * Created by Ivo Zeba on 5/14/17.
 *
 * A collection of helper methods for drawing on the canvas
 */

export class CanvasHelper {
    /**
     * Draws a line between two points
     *
     * @param context Canvas context
     * @param startX X-coordinate of first point
     * @param startY Y-coordinate of first point
     * @param endX X-coordinate of second point
     * @param endY Y-coordinate of second point
     * @param color Hex value of the color of the line
     */
    static drawLine(context, startX, startY, endX, endY, color) {
        context.beginPath();
        context.strokeStyle = color;
        context.moveTo(startX, startY);
        context.lineTo(endX, endY);
        context.stroke();
        context.closePath();
    };

    /**
     * Draws and fills square
     *
     * @param context Canvas context
     * @param startX X-coordinate of first point
     * @param startY Y-coordinate of first point
     * @param squareSideLength
     * @param strokeColor Color of the edges
     * @param fillColor Color of the square
     */
    static drawSquare(context, startX, startY, squareSideLength, strokeColor, fillColor) {
        context.beginPath();
        context.clearRect(startX, startY, squareSideLength, squareSideLength);
        context.rect(startX, startY, squareSideLength, squareSideLength);
        context.strokeStyle = strokeColor;
        context.fillStyle = fillColor;
        context.fill();
        context.stroke();
        context.closePath();
    };
}