# Canvas Calendar Chart

## What is it?

It's a chart that looks like this and uses canvas instead of svg:

![Alt text](https://gitlab.com/thebeardedone/Canvas-Calendar-Chart/raw/master/img/cal1.png "Calendar with alternate color scheme")
![Alt text](https://gitlab.com/thebeardedone/Canvas-Calendar-Chart/raw/master/img/cal2.png "13 month calendar with primary color scheme")

## Why?

I needed a calendar chart for a financial explorative tool I am building. From what I saw there were 3 options: google charts 
(dependent on google and can't run it offline? no thanks), ZingChart (not exactly free for everyone) and d3 (I don't like depending on d3 even
though I think it's pretty cool). Although it should not matter for these types of charts, canvas is faster than svg.

## Quick How To

In your html define a div with an id:

```
<div id="calendar"></div>
```

Import `dist/calendar.bundle.js` as follows:

```
<script type="text/javascript" src="dist/calendar.bundle.js"></script>
```

In a script tag or in another javascript file define at least the minimum configuration:

```
var config = {
    id: "calendar",
    chart: {
        startDate: "2016-01-01",
        endDate: "2016-12-31"
    }
};
```

And pass it to the create chart function:

```
var calendar = CalendarFactory.createChart(config);
```

## Configuration options

### Custom Colors

To define a custom set of colors, define the colors in an array of hexadecimal strings as follows in the config object:

```
var config = {
    id: "calendar",
    color: {
        range: ["#D7191C",
                "#FC8D59",
                "#FFFFBF",
                "#91BFDB",
                "#2C7BB6"
        ]
    },
    .
    .
 }
```

### Date range

To set the number of months shown, pass a start date and end date to the chart object. Additionally one can set skip weekends
to true to not show Saturday and Sunday.

```
var config = {
    id: "calendar",
    chart: {
        startDate: "2016-01-01",
        endDate: "2016-12-31",
        skipWeekend: true
    },
    .
    .
 }
```

### Passing data

Pass in an object array of the following type to visualize it:

```
var dataObjects = [ { date: ..., value: ... }, ...,... , ... ]
```

```
var config = {
    id: "calendar",
    data: dataObjects,
    .
    .
 }
```

### Events

There are two types of event handlers: onHover and onClick. The respective callback functions for these handlers look as
follows:

```
function callBack(point, calendar, event) {
    // do something here
}
```

Point is either the data point that is being hovered on or that has been clicked on. Calendar is the calendar object in
case you need to access the canvas. The event is the original event that was fired off and used to get the data point.

#### onHover

When hovering over a date in the canvas, we can pass a callback function to format either the date or value. The
following is an example of a of a date being formatted in "YYYY-MM-DD" and the value being fixed to 3 decimal places:

```
var config = {
    id: "calendar",
    .
    .
    events: {
        onHover: {
            date: function(point) {
                return "Date: " + point.date.getFullYear() + "-" + (point.date.getMonth() < 9 ? "0" + (point.date.getMonth() + 1) : (point.date.getMonth() + 1)) + "-" + (point.date.getDate() < 10 ? "0" + point.date.getDate() : point.date.getDate());
            },
            value: function (point) {
                return "New Value: " + point.value.toFixed(3);
            }
        }
    }
 }
```

NOTE: The original intention of this is to be able to format the values as you please.

#### onClick

The following is an example of a fancy date selector which can be used to save what dates have been selected in an array:

```
var selectedData = [];

var config = {
    id: "calendar",
    .
    .
    events: {
        onClick: function(point, calendar, event) {
             let index = selectedData.indexOf(point);
             if(index === -1) {
                 calendar.getCanvasHelper().drawSquare(calendar.context, point.x + 1, point.y + 1, calendar.config.style.squareSideLength - 2, calendar.config.color.dateOutline, "#A6D96A");
                 selectedData.push(point);
             } else {
                 calendar.getCanvasHelper().drawSquare(calendar.context, point.x, point.y, calendar.config.style.squareSideLength, calendar.config.color.dateOutline, "#FFFFFF");
                 selectedData.splice(index, 1);
             }
             calendar.drawMonthBorder(new Date(point.date), point.x, point.y);
         }
    }
 }
```

![Alt text](https://gitlab.com/thebeardedone/Canvas-Calendar-Chart/raw/master/img/dateselector.png "Fancy date selector")

### Deleting an object

To delete the calendar just call destroy and set the object to null.

```
calendar.destroy();
calendar = null;
```

### Other Options

There are several other objects which may be overwritten but are untested such as the days of the week, names of months,
and width/height of canvas. These can be found in the [default configuration](https://gitlab.com/thebeardedone/Canvas-Calendar-Chart/raw/master/src/defaultConfigurations.js).

## Additional Notes

In case that you like this, feel free to share what you used it for! If you don't like it, create a better one and share
it!